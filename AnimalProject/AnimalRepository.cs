﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnimalProject
{
   public  interface IAnimalRepository
    {
        void Add(Animal a);
        void Delete(Animal a);
        void ModifyDescription(Animal a, String description);
        Animal GetById(int id);
        IEnumerable<Animal> GetAll();
    }
   public class AnimalRepository : IAnimalRepository
    {
        readonly List<Animal> _animals = new List<Animal>();
        public void Add(Animal a)
        {
            _animals.Add(a);
        }
        public void Delete(Animal a)
        {
            _animals.Remove(a);
        }

        public IEnumerable<Animal> GetAll()
        {
            return _animals;
        }

        public Animal GetById(int id)
        {
            return _animals.SingleOrDefault(x => x.Id == id);
        }

        public void ModifyDescription(Animal a, String description )
        {
            a.Description = description;
        }
    }
}
